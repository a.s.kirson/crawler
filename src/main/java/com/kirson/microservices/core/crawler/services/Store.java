package com.kirson.microservices.core.crawler.services;

import com.kirson.microservices.core.crawler.model.Sku;

import java.io.IOException;


public interface Store {
    Sku getSku(String storeName, String SkuUrl) throws IOException;

    boolean compareStoreName(String storeName) ;
}
