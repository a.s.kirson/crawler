package com.kirson.microservices.core.crawler;

import java.util.HashMap;
import java.util.Map;

public class StoreSkuUrls
{
  /**
   * Giperglobus
   * Potato_1kg                     = Картофель, 1 кг
   * TomatoReadBunch_1kg            = Томаты красные на ветке, 1 кг
   * PinkTomato_1kg                 = Томаты розовые, 1 кг
   * MediumFruitedCucumbers_1kg     = Огурцы среднеплодные, 1 кг
   * CucumberSmooth_1kg             = Огурец гладкий, 1 кг
   * WhiteCabbage_1kg               = Капуста белокочанная, 1 кг
   * Beetroot_1kg                   = Свёкла, 1 кг
   * Carrot_1kg                     = Морковь, 1 кг
   * Onion_1kg                      = Лук репчатый , 1 кг
   * Garlic_1kg                     = Чеснок, 1 кг
   * Sugar_1kg                      = Сахар-песок, 1 кг
   * TableSaltStone_1kg             = Соль поваренная пищевая каменная, 1 кг
   * Butter_180g                    = Масло сливочное Брест-Литовск 82,5%, 180 г
   * Flour_2kg                      = Мука пшеничная Makfa высший сорт, 2 кг
   * PastaTwisted_450g              = Макаронные изделия Витой рожок Шебекинские, 450 г
   * Buckwheat_groats_900g          = Крупа гречневая Мистраль ядрица, 900 г
   * BeefLegBone_1kg                = Говяжья нога на кости, 1 кг
   * PorkHam_1kg                    = Свиной окорок, 1 кг
   * BroilerChickenCooledSkin_1kg   = Бедро цыплёнка-бройлера охлаждённое Моссельпром с кожей, 1 кг
   * PasteurizedMilk_3.4-4.5%_930ml = Молоко пастеризованное Простоквашино отборное 3,4-4,5%, 930 мл
   * UltraPasteurizedMilk_1l        = Молоко ультрапастеризованное Parmalat 3,5%, 1 л
   * SunflowerOilRefined_1l         = Масло подсолнечное Золотая Семечка рафинированное, 1 л
   */
  private static final Map<String, String> GIPERGLOBUS_URLS = new HashMap<>()
  {{
    put("Potato_1kg", "https://www.globus.ru/products/26826_KG/");
    put("TomatoReadBunch_1kg", "https://www.globus.ru/products/389976_KG/");
    put("PinkTomato_1kg", "https://www.globus.ru/products/254837_KG/");
    put("MediumFruitedCucumbers", "https://www.globus.ru/products/360351_KG/");
    put("CucumberSmooth_1kg", "https://www.globus.ru/products/100710_KG/");
    put("WhiteCabbage_1kg", "https://www.globus.ru/products/143_KG/");
    put("Beetroot_1kg", "https://www.globus.ru/products/55712_KG/");
    put("Carrot_1kg", "https://www.globus.ru/products/100383_KG/");
    put("Onion_1kg", "https://www.globus.ru/products/2550_KG/");
    put("Garlic_1kg", "https://www.globus.ru/products/55766_KG/");
    put("Sugar_1kg", "https://www.globus.ru/products/178430_KG/");
    put("TableSaltStone_1kg", "https://www.globus.ru/products/371185_ST/");
    put("Butter_180g", "https://www.globus.ru/products/204352_ST/");
    put("Flour_2kg", "https://www.globus.ru/products/52573_ST/");
    put("PastaTwisted_450g", "https://www.globus.ru/products/22061_ST/?sphrase_id=7278735");
    put("Buckwheat_groats_900g", "https://www.globus.ru/products/22205_ST/");
    put("BeefLegBone_1kg", "https://www.globus.ru/products/44679_KG/");
    put("PorkHam_1kg", "https://www.globus.ru/products/44590_KG/");
    put("BroilerChickenCooledSkin_1kg", "https://www.globus.ru/products/81763_KG/");
    put("PasteurizedMilk_3.4-4.5%_930ml", "https://www.globus.ru/products/27688_ST/");
    put("UltraPasteurizedMilk_1l", "https://www.globus.ru/products/26730_ST/");
    put("SunflowerOilRefined_1l", "https://www.globus.ru/products/27260_ST/");
  }};

  /**
   * Aushan
   * Potato_1kg                     = Картофель белый, 1 кг
   * TomatoReadBunch_1kg            = Томаты Банч, 1 кг
   * PinkTomato_1kg                 = Томаты розовые, 1 кг
   * CucumberSmooth_1kg             = Огурцы среднеплодные гладкие, вес, 1 кг
   * WhiteCabbage_1kg               = Капуста белокочанная, 1 кг
   * Beetroot_1kg                   = Свёкла, 1 кг
   * Carrot_1kg                     = Морковь, 1 кг
   * Onion_1kg                      = Лук репчатый, 1 кг
   * Garlic_1kg                     = Чеснок, 1 кг
   * Sugar_1kg                      = Сахар-песок, 1 кг
   * TableSaltStone_1kg             = Соль поваренная пищевая каменная, 1 кг
   * Butter_180g                    = Масло сливочное Брест-Литовск 82,5%, 180 г
   * Flour_2kg                      = Мука пшеничная Makfa высший сорт, 2 кг
   * PastaTwisted_450g              = Макаронные изделия Витой рожок Шебекинские, 450 г
   * Buckwheat_groats_900g          = Крупа гречневая Мистраль ядрица, 900 г
   * Boneless_chilled_pork_ham_1kg  = Окорок свиной бескостный охлажденный, 1 кг
   * BroilerChickenCooledSkin_1kg   = Бедро цыпленка-бройлера «Каждый день», 1 кг
   * PasteurizedMilk_3.4-4.5%_930ml = Молоко пастеризованное Простоквашино отборное 3,4-4,5%, 930 мл
   * UltraPasteurizedMilk_1l        = Молоко ультрапастеризованное Parmalat 3,5%, 1 л
   * SunflowerOilRefined_1l         = Масло подсолнечное Золотая Семечка рафинированное, 1 л
   */
  private static final Map<String, String> AUSHAN_URLS = new HashMap<>()
  {{
    put("Potato_1kg", "https://www.auchan.ru/product/kartofel_belyy_ves/");
    put("TomatoReadBunch_1kg", "https://www.auchan.ru/product/tomat_na_vetke_ves/");
    put("PinkTomato_1kg", "https://www.auchan.ru/product/tomat_rozovyy_ves/");
    put("CucumberSmooth_1kg", "https://www.auchan.ru/product/ogurec_sredniy_gladkiy_ves/");
    put("WhiteCabbage_1kg", "https://www.auchan.ru/product/kapusta_bk_ves/");
    put("Beetroot_1kg", "https://www.auchan.ru/product/svekla_ves/");
    put("Carrot_1kg", "https://www.auchan.ru/product/morkov_ves/");
    put("Onion_1kg", "https://www.auchan.ru/product/luk_repchatyy_ves/");
    put("Garlic_1kg", "https://www.auchan.ru/product/chesnok_ves/");
    put("Sugar_1kg", "https://www.auchan.ru/product/saharnyy-pesok-russkiy-sahar-1-kg/");
    put("TableSaltStone_1kg", "https://www.auchan.ru/product/sol-tyretskiy-solerudnik-pishchevaya-1-kg/");
    put("Butter_180g", "https://www.auchan.ru/product/maslo-brest-litovsk-sladko-slivochnoe-nesolenoe-82-5-180g/");
    put("Flour_2kg", "https://www.auchan.ru/product/muka-pshenichnaya-makfa-vysshtiy-sort-2-kg/");
    put("PastaTwisted_450g", "https://www.auchan.ru/product/makaronnye-izdeliya-shebekinskie-rozhok-vitoy-450-g/");
    put("Buckwheat_groats_900g", "https://www.auchan.ru/product/grechka-mistral-yadrica-900-g/");
    put("Boneless_chilled_pork_ham_1kg", "https://www.auchan.ru/product/okorok_1_tf_cm_ohl/");
    put("BroilerChickenCooledSkin_1kg", "https://www.auchan.ru/product/kd-bedro-cb-ohl-1/");
    put("PasteurizedMilk_3.4-4.5%_930ml", "https://www.auchan.ru/product/moloko-prostokvashino-pasterizovannoe-otbornoe-3-4-4-5-930-ml");
    put("UltraPasteurizedMilk_1l", "https://www.auchan.ru/product/moloko-parmalat-ultrapasterizovannoe-3-5-1-l");
    put("SunflowerOilRefined_1l", "https://www.auchan.ru/product/maslo-zolotaya-semechka-rafinirovannoe-dezodorirovannoe-1-l");
  }};

  public static final Map<String, Map<String, String>> STORES_URLS = new HashMap<>()
  {{
    put("Giperglobus", GIPERGLOBUS_URLS);
    put("Aushan", AUSHAN_URLS);
  }};
}
