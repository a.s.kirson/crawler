package com.kirson.microservices.core.crawler.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kirson.microservices.core.crawler.StoreSkuUrls;
import com.kirson.microservices.core.crawler.model.Sku;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ScheduledScrapingSkusService
{
  private final List<Store>  stores;
  private final StreamBridge streamBridge;

  private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ScheduledScrapingSkusService.class);

  public ScheduledScrapingSkusService(List<Store> stores, StreamBridge streamBridge)
  {
    this.stores       = stores;
    this.streamBridge = streamBridge;
  }

  @Scheduled(cron = "* 1 * * * *")
  public void startScrape()
  {
    final ExecutorService executor = Executors.newFixedThreadPool(6);

    for (String storeName : StoreSkuUrls.STORES_URLS.keySet()) {
      Map<String, String> storeUrls = StoreSkuUrls.STORES_URLS.get(storeName);
      for (String url : storeUrls.values()) {
        stores.stream()
            .filter(store -> store.compareStoreName(storeName))
            .forEach(store -> executor.submit(() -> {
              Sku sku = store.getSku(storeName, url);

              if (sku != null) {
                String skuJson = new ObjectMapper().writeValueAsString(sku);
                sendMessage(skuJson);
              }

              return sku;
            }));
      }
    }

    executor.shutdown();
  }

  @Retryable
  private void sendMessage(String skuJson)
  {
    Message<String> message = MessageBuilder.withPayload(skuJson)
        .build();

    LOG.info("Send message {}", skuJson);

    streamBridge.send("products-out-0", message);
  }
}
