package com.kirson.microservices.core.crawler.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.concurrent.Callable;

public record Sku(String name, String storeName, double price, long createTime) implements Callable<String>
{
  @Override
  public String call() throws Exception
  {
    return new ObjectMapper().writeValueAsString(this);
  }
}
