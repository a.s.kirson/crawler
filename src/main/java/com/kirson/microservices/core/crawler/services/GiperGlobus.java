package com.kirson.microservices.core.crawler.services;

import com.kirson.microservices.core.crawler.model.Sku;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;

@Component("GiperGlobus")
public class GiperGlobus implements Store
{
  private static final String           STORE_NAME = "Giperglobus";
  private static final org.slf4j.Logger LOG        = LoggerFactory.getLogger(GiperGlobus.class);

  public Sku getSku(String storeName, String url)
  {
    try {
      if (storeName.equals(STORE_NAME)) {
        final Document doc            = Jsoup.connect(url).get();
        final String   entier         = doc.body().getElementsByClass("catalog-detail__item-price-actual-main").text();
        final String   fractionalPart = doc.body().getElementsByClass("catalog-detail__item-price-actual-sub").text();
        final String   name           = doc.body().getElementsByClass("catalog-detail__title-h1").textNodes().get(0).getWholeText();
        final double   price          = Double.parseDouble(entier.concat(".").concat(fractionalPart));

        LOG.info("name {}, price {}", name, price);

        return new Sku(name, STORE_NAME, price, Instant.now().toEpochMilli());
      }
    } catch (IOException e) {
      LOG.error(e.getMessage());
      throw new IllegalArgumentException();
    }

    return null;
  }


  @Override
  public boolean compareStoreName(String storeName)
  {
    return STORE_NAME.equals(storeName);
  }
}
